package app.repository;

import app.entity.Cat;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CatRepository extends CrudRepository<Cat, Long> {
}
