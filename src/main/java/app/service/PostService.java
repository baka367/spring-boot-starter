package app.service;

import app.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import app.repository.PostRepository;

import java.util.Optional;

@Component
public class PostService {
    @Autowired
    private PostRepository postRepository;

    public void createPost(String title, String contents) {
        Post post = new Post();
        post.setTitle(title);
        post.setContents(contents);
        post.setEnabled(true);
        post.setState(1);
        postRepository.save(post);
    }
    @Nullable
    public Optional<Post> getPost(long id) {
        return postRepository.findById(id);

    }
}
