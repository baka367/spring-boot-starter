package app.service;

public class StatusService {
    String apiVersion = "";

    public void setVersion(String version) {
        this.apiVersion = version;
    }

    public app.dataObject.Status getStatus() {
        app.dataObject.Status status = new app.dataObject.Status();
        status.setApiKey("1234ASD");
        status.setApiVersion(apiVersion);
        status.setOpen(true);
        return status;
    }
}
