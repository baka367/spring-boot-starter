package app.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AbstractController {
//    public JSONPObject
    protected ApplicationContext context;

    public AbstractController() {
        this.context = new ClassPathXmlApplicationContext("Beans.xml");
    }
}
