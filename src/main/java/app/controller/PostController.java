package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.service.PostService;

import java.util.HashMap;

@RestController
@RequestMapping(path="post/")
public class PostController extends AbstractController{
    @Autowired
    PostService postService;

    @RequestMapping(path="create")
    public @ResponseBody
    HashMap<String, Object> createPost(@RequestParam String title
            , @RequestParam String contents) {

        postService.createPost(title, contents);

        HashMap<String, Object> response = new HashMap<>();

        response.put("created", true);
        return response;
    }

    @RequestMapping(path="get")
    public @ResponseBody HashMap<String, Object> getPost(@RequestParam long id) {
        HashMap<String, Object> response = new HashMap<>();
        response.put("post", postService.getPost(id));
        return response;
    }
}
