package app.controller;

import app.entity.Cat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {
    @RequestMapping("/")
    public String home() {

        Cat cat = new Cat();
        cat.setWeight(1);
        return "Hello Docker World";
    }
}
