package app.controller;

import app.dataObject.Status;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import app.service.StatusService;

@RestController
public class StatusController extends AbstractController {

    @RequestMapping("/status")
    public Status status() {
        StatusService service = (StatusService) context.getBean("status");


        return service.getStatus();
    }
}
