package app.dataObject;

import java.util.HashMap;

public class Status {
    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public HashMap getFeatureList() {
        return featureList;
    }

    public void setFeatureList(HashMap featureList) {
        this.featureList = featureList;
    }

    String apiVersion;
    boolean isOpen;
    String apiKey;
    HashMap featureList;
}
